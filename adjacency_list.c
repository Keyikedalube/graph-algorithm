/*
 * Graph: adjacency list
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>




typedef struct node_data {
	short  data;
	size_t link_count;

	struct node_data **link;
} node;

short  vertex_count = 0;
node **vertex       = NULL;




short get_decimal_input(int lower_limit, int upper_limit)
{
	int input = 0;

	while (1) {
		char line[2];
		if (fgets(line, sizeof(line), stdin) && sscanf(line, "%i", &input)) {
			if (input >= lower_limit && input <= upper_limit) {
				// Discard newline character
				char *p;
				if ((p = strchr(line, '\n')))
					*p = 0;
				else {
					scanf("%*[^\n]");
					scanf("%*c");
				}
				break;
			} else if (!strcmp(line, "\n"))
				printf("Invalid input %i. Try again: ", input);
		}
	}

	return(input);
}


void delete_vertexes()
{
	for (size_t i = 0; i < vertex_count; i++) {
		if (vertex[i]->link) free(vertex[i]->link);

		free(vertex[i]);

		vertex[i] = NULL;
	}
}


node *add_vertex(int data)
{
	node *vertex;

	if ( !(vertex = malloc(sizeof(*vertex))) ) {
		puts("Memory allocation faild! Exiting program...");
		delete_vertexes();
		exit(1);
	}

	vertex->data       = data;
	vertex->link_count = 0;
	vertex->link       = NULL;

	return(vertex);
}


void add_edge(node *v1, node *v2)
{
	if (!v1->link) {

		// Allocate new array
		if ( !(v1->link = malloc(sizeof(*v1))) ) {
			puts("Array allocation failed! Exiting program...");
			delete_vertexes();
			exit(1);
		}

	} else {

		// Reallocate array
		if ( !(v1->link = realloc(v1->link, sizeof(*v1) * (v1->link_count+1))) ) {
			puts("Array reallocation failed! Exiting program...");
			delete_vertexes();
			exit(1);
		}

	}

	v1->link[v1->link_count] = v2;

	v1->link_count++;

	v1->link[v1->link_count] = NULL;
}


void print_vertex(node *vertex)
{
	if (!vertex) {
		puts("Empty vertex");
		return;
	}

	// First vertex
	printf("%i", vertex->data);

	size_t index = 0;

	// Array links
	while (index < vertex->link_count) {
		printf("->%i", vertex->link[index]->data);
		index++;
	}

	puts("");
}


int main()
{
	puts("--- This program takes in only single digit input. ---");
	puts("---                                                ---");
	puts("--- If more than one digit input is given, program ---");
	puts("--- will scan the first preceding digit and may    ---");
	puts("--- proceed to next digit until condition is met   ---\n\n");




	printf("Enter no. of vertexes (2-9): ");
	vertex_count = get_decimal_input(2, 9);

	// Create and allocate vertexes
	if ( !(vertex = malloc(sizeof(*vertex) * vertex_count)) ) {
		puts("Memory allocation faild! Exiting program...");
		exit(1);
	}

	for (size_t i = 0; i < vertex_count; i++)
		vertex[i] = add_vertex(rand() % 10);

	// Create edges
	printf("\nEnter no. of edge (1-%i): ", vertex_count-1);
	short edge = get_decimal_input(1, vertex_count-1);

	const int UPPER_LIMIT = vertex_count - 1;
	for (size_t i = 0; i < edge; i++) {
		printf("\nFor edge %lu:\n", i);

		printf("\tEnter source vertex (0-%i): ", UPPER_LIMIT);
		short source = get_decimal_input(0, UPPER_LIMIT);

		printf("\tEnter destination vertex (0-%i): ", UPPER_LIMIT);
		short destination = get_decimal_input(0, UPPER_LIMIT);

		add_edge(vertex[source], vertex[destination]);
	}

	// Print all vertexes
	puts("\nAdjacency list:");
	for (size_t i = 0; i < vertex_count; i++)
		print_vertex(vertex[i]);

	delete_vertexes();

	return(0);
}
