/*
 * Basic directed graph algorithm
 *
 * Nodes operation:
 * - Link
 * - Unlink
 * - Print
 *
 * Referenced from https://codereview.stackexchange.com/questions/250064/graph-implementation-in-c
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>




typedef struct node_data {
	int    data;
	size_t link_count;

	struct node_data **link;
} node;




void null_exit(node *n)
{
	if (n) return;

	puts("Node empty!");
	exit(1);
}

void delete_node(node **n)
{
	null_exit(*n);

	if ((*n)->link_count > 0)
		free((*n)->link);

	free(*n);

	*n = NULL;
}

void allocate_new_node(node **n)
{
	*n = malloc(sizeof(**n));

	if (*n) return;

	puts("Memory allocation failed! Exiting program...");
	exit(1);
}

void initialize_node_data(node *n)
{
	n->data       = rand() % 10;
	n->link_count = 0;
	n->link       = NULL;
}

void increase_link_count(node *n)
{
	if (n->link_count == 0)
		n->link = malloc(sizeof(*n));
	else
		n->link = realloc(n->link, sizeof(*n) * (n->link_count+1));

	if (n->link) return;

	if (n->link_count == 0)
		puts("New array allocation failed! Exiting program...");
	else
		puts("Array resize failed! Exiting program...");
	exit(1);
}

void link_node(node *n1, node *n2)
{
	null_exit(n1);
	null_exit(n2);

	increase_link_count(n1);

	n1->link[n1->link_count] = n2;

	n1->link_count++;

	n1->link[n1->link_count] = NULL;
}

void unlink_node(node *n1, node *n2)
{
	null_exit(n1);
	null_exit(n2);

	size_t link_count = n1->link_count;

	while (link_count > 0) {
		link_count--;

		if (n1->link[link_count] == n2) {

			if (n1->link_count > 0) {
				// Replace this node with the last node
				n1->link[link_count] = n1->link[n1->link_count-1];
				// Then resize the array
				n1->link = realloc(n1->link, sizeof(*n1) * (n1->link_count-1));
			} else
				free(n1->link);

			n1->link_count--;
			break;
		}
	}
}

void print(const char *NODE_ID, node *n)
{
	null_exit(n);

	printf("\t%s: %i", NODE_ID, n->data);

	size_t link_count = n->link_count;

	if (link_count == 0) {
		puts("");
		return;
	}

	// We have array of node links, print their data one by one
	while (link_count > 0) {
		link_count--;
		printf("->%i", n->link[link_count]->data);
	}

	puts("");
}

int main()
{
	srand(time(NULL));

	node *n1, *n2, *n3;

	allocate_new_node(&n1);
	allocate_new_node(&n2);
	allocate_new_node(&n3);

	initialize_node_data(n1);
	initialize_node_data(n2);
	initialize_node_data(n3);

	puts("Initial nodes data:");
	print("n1", n1);
	print("n2", n2);
	print("n3", n3);

	puts("\nAfter linking n1 and n2");
	link_node(n1, n2);
	print("n1", n1);
	print("n2", n2);
	print("n3", n3);


	puts("\nAfter linking n1 and n3");
	link_node(n1, n3);
	print("n1", n1);
	print("n2", n2);
	print("n3", n3);

	//puts("\nAfter unlinking n1 and n2");
	//unlink_node(n1, n2);
	//print("n1", n1);
	//print("n2", n2);
	//print("n3", n3);

	puts("\nAfter unlinking n1 and n3");
	unlink_node(n1, n3);
	print("n1", n1);
	print("n2", n2);
	print("n3", n3);

	delete_node(&n1);
	delete_node(&n2);
	delete_node(&n3);

	//print("n1", n1);
	//print("n2", n2);
	//print("n3", n3);

	return(0);
}
