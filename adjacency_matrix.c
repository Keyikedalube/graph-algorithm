/*
 * Graph: adjacency matrix
 */

#include <stdio.h>
#include <string.h>

int get_decimal_input(int lower_limit, int upper_limit)
{
	int input = 0;

	while (1) {
		char line[2];
		if (fgets(line, sizeof(line), stdin)) {
			if (sscanf(line, "%i", &input) && input >= lower_limit && input <= upper_limit) {
				// Discard newline character
				char *p;
				if ((p = strchr(line, '\n')))
					*p = 0;
				else {
					scanf("%*[^\n]");
					scanf("%*c");
				}
				break;
			} else if (!strcmp(line, "\n"))
				printf("Invalid input %i. Try again: ", input);
		}
	}

	return(input);
}

int main()
{
	puts("--- This program takes in only single digit input. ---");
	puts("---                                                ---");
	puts("--- If more than one digit input is given, program ---");
	puts("--- will scan the first preceding digit and may    ---");
	puts("--- proceed to next digit until condition is met   ---\n\n");

	printf("Enter no. of vertexes (3-9): ");
	int vertex = get_decimal_input(3, 9);

	printf("\nEnter no. of edge (1-%i): ", vertex-1);
	int edge = get_decimal_input(1, vertex-1);

	short unsigned matrix[vertex][vertex];

	// Initialize 2D matrix to 0
	for (size_t i = 0; i < vertex; i++)
		for (size_t j = 0; j < vertex; j++)
			matrix[i][j] = 0;


	for (size_t i = 1; i <= edge; i++) {
		printf("\nEnter row no (0-%i): ", vertex-1);
		int row = get_decimal_input(0, vertex-1);

		printf("Enter col no (0-%i): ", vertex-1);
		int col = get_decimal_input(0, vertex-1);

		matrix[row][col] = 1;
		//matrix[col][row] = 1; // Undirected graph
	}

	puts("\nDirected graph:");
	for (size_t i = 0; i < vertex; i++) {
		for (size_t j = 0; j < vertex; j++)
			printf(" %i", matrix[i][j]);
		puts("");
	}

	return(0);
}
